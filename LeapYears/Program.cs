﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using LeapYearsCore.Services;




namespace LeapYears
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection().AddTransient<ILeapYearService, LeapYearService>().BuildServiceProvider();

            ILeapYearService _leapYearService = serviceProvider.GetService<ILeapYearService>();

            Console.WriteLine("Please enter a year to check if it is a Leap year:");

            string input = Console.ReadLine();

            int yearVal;
            if(!Int32.TryParse(input, out yearVal))
            {
                Console.WriteLine("Unvalid year format!");
            }

            try
            {
                bool isLeapYear =_leapYearService.IsLeapYear(yearVal);

                if(isLeapYear)
                    Console.WriteLine("Leap year");
                else
                    Console.WriteLine("Ordinary year");
            }
            catch(LeapYearsCore.Exceptions.UnvalidYearFormatException ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
