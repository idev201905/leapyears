﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeapYearsCore.Exceptions
{
    public class UnvalidYearFormatException:Exception
    {
        public UnvalidYearFormatException() : base() { }
        public UnvalidYearFormatException(string message) : base(message) { }
    }
}
