﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeapYearsCore.Services
{
    public interface ILeapYearService
    {
        bool FirstCriteriaCheck(int year);
        bool SecondCriteriaCheck(int year);
        bool ThirdCriteriaCheck(int year);
        bool FourthCriteriaCheck(int year);

        bool IsLeapYear(int year);
    }
}
