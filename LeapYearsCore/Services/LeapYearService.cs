﻿using System;
using System.Collections.Generic;
using System.Text;
using LeapYearsCore.Exceptions;



namespace LeapYearsCore.Services
{
    public class LeapYearService : ILeapYearService
    {

        public virtual bool FirstCriteriaCheck(int year)
        {
            if (year % 400 == 0)
                return true;

            return false;
        }

        public bool SecondCriteriaCheck(int year)
        {
            if (year % 100 == 0 && year % 400 != 0)
                return false;

            return true;
        }

        public bool ThirdCriteriaCheck(int year)
        {
            if (year % 4 == 0 && year % 100 != 0)
                return true;

            return false;
        }

        public bool FourthCriteriaCheck(int year)
        {
            if (year % 4 != 0)
                return false;

            return true;
        }

        public virtual bool IsLeapYear(int year)
        {
            // We will consider a negative year an unvalid one and throw an exception
            if (year < 0)
                throw new UnvalidYearFormatException("Parameter year must be greater or equal to zero!");

            if (FirstCriteriaCheck(year))
                return true;

            if (ThirdCriteriaCheck(year))
                return true;

            //No need to check other criterias as they verify no leap years
  
            return false;
        }
    }
}
