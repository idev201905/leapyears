﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeapYearsCore.Services
{
    //We Inject this class when we reach 31/12/3999 :)
    public class LeapYearService4k : LeapYearService
    {

        public override bool FirstCriteriaCheck(int year)
        {
            if (year % 400 == 0 && year % 4000 != 0)
                return true;

            return false;
        }

     

    
    }
}
