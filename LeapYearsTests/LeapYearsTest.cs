﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Microsoft.Extensions.DependencyInjection;
using LeapYearsCore.Services;
using LeapYearsCore.Exceptions;


namespace LeapYearsTests
{

    [TestFixture]
    public class LeapYearsTest
    {
        public readonly ILeapYearService _leapYearService;
        public LeapYearsTest()
        {
            var serviceProvider = new ServiceCollection().AddTransient<ILeapYearService, LeapYearService>().BuildServiceProvider();

            _leapYearService = serviceProvider.GetService<ILeapYearService>();
        }

        [TestCase(2000)]
        [TestCase(1600)]
        [TestCase(400)]
        public void FirstCriteriaTest(int val)
        {
            bool didPass = _leapYearService.FirstCriteriaCheck(val);

            Assert.AreEqual(true, didPass);
        }

        [TestCase(1700)]
        [TestCase(1800)]
        [TestCase(1900)]
        public void SecondCriteriaTest(int val)
        {
            bool didPass = _leapYearService.SecondCriteriaCheck(val);

            Assert.AreNotEqual(true, didPass);
        }

        [TestCase(2008)]
        [TestCase(2016)]
        [TestCase(2012)]
        public void ThirdCriteriaTest(int val)
        {
            bool didPass = _leapYearService.ThirdCriteriaCheck(val);

            Assert.AreEqual(true, didPass);
        }

        [TestCase(2017)]
        [TestCase(2018)]
        [TestCase(2019)]
        public void FourthCriteriaTest(int val)
        {
            bool didPass = _leapYearService.FourthCriteriaCheck(val);

            Assert.AreNotEqual(true, didPass);
        }

        [Test]
        public void ExceptionUnvalidFormatTest()
        {
            Assert.Throws<UnvalidYearFormatException>(() => _leapYearService.IsLeapYear(-300));
        }

        [TestCase(2008)]
        [TestCase(2000)]
        [TestCase(2016)]
        public void IsLeapYearPositiveTest(int val)
        {
            bool didPass = _leapYearService.IsLeapYear(val);

            Assert.AreEqual(true, didPass);
        }


        [TestCase(2100)]
        [TestCase(1800)]
        [TestCase(2017)]
        public void IsLeapYearNegativeTest(int val)
        {
            bool didPass = _leapYearService.IsLeapYear(val);

            Assert.AreNotEqual(true, didPass);
        }


       

    }
}
