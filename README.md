# Leap Years

A C#.NET Console program to check if a year is a leap year.

Developed using Visual Studio 2019.

Dependencies:

NUnit

NUnit3TestAdapter

Microsoft.Extensions.DependencyInjection